package com.example.eheca.showlistapp

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class ShowAdapter(shows: MutableList<Show>) : RecyclerView.Adapter<ShowAdapter.ViewHolder>(){
    //Show list
    private val shows: MutableList<Show> = shows

    //Inflate the show_card layout
    override fun onCreateViewHolder(viewGroup: ViewGroup,position: Int): ViewHolder {
        val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.show_card, viewGroup, false)
        return ViewHolder(itemView)
    }
    //Get the shows list size
    override fun getItemCount(): Int = shows.size
    //Bind the show data in show_card
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bindShowModel(shows[position])
    }
    //Add a new show in shows list
    fun addShows(shows: MutableList<Show>){
        clear()
        this.shows.addAll(shows)
        notifyDataSetChanged()
    }

    fun clear(){
        shows.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var show: Show ?= null
        var imageShow_ImageView: ImageView = itemView.findViewById(R.id.imageShow_ImageView)
        var showName_TextView: TextView = itemView.findViewById(R.id.showName_TextView)

        init {
            //Open a ew Activity if the user click the show_card
            itemView.setOnClickListener{
                var position: Int = adapterPosition
                //Open the ShowActivity and put all show values
                var intent = Intent(it.context, ShowActivity::class.java)
                intent.putExtra(ShowActivity.id_key, show?.id)
                intent.putExtra(ShowActivity.name_key, show?.name)
                intent.putExtra(ShowActivity.image_key, show?.imageUrl)
                intent.putExtra(ShowActivity.description_key, show?.description)
                intent.putExtra(ShowActivity.url_key, show?.url)
                intent.putExtra(ShowActivity.time_key, show?.time)
                intent.putExtra(ShowActivity.network_key, show?.networkName)

                var days = ""
                for(day in show?.days!!)
                    days+= "$day "

                intent.putExtra(ShowActivity.days_key, days)

                it.context.startActivity(intent)
            }
        }
        //Bind the show data
        fun bindShowModel(show: Show){
            this.show = show
            //Load the show image
            if(!show.imageUrl.isEmpty())
                Picasso.get().load(show.imageUrl).into(imageShow_ImageView)
            else
                //If show image is empty load the app icon
                Picasso.get().load(R.drawable.app_icon).into(imageShow_ImageView)
            //Load the title image
            showName_TextView.text = show.name
        }
    }
}