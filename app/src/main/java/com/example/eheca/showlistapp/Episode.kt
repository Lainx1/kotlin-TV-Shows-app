package com.example.eheca.showlistapp

//Episodes Model
class Episode {
    var number : Int = 0
    var season: Int = 0
    var name: String = ""
}